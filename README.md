# Project Name
Crud App

# Description
An Application using MEAN Stack which perform Database Operation CRUD(create, read, update and delete) in MongoDB.

# Project setup
## Step1: 
   Download node.js
## Step2: 
  npm install -g @angular/cli
  #https://angular.io/guide/setup-local
## Step3:
  Download and Install MongoDB
  
  
## Install few dependencies for backend
  npm install express body-parser mongoose nodemon concurrently --save
  
# How to execute Project
1 . start mongodb server<br>
2. run ng serve on cmd to AngularJS<br>
3. run nodemon server on cmd<br>

Go to http://localhost:4200 
  
  
  

